#ifndef TRIGT1CAL_CMXCPHitsCOLLECTION_H
#define TRIGT1CAL_CMXCPHitsCOLLECTION_H

#include "AthContainers/DataVector.h"
#include "TrigT1CaloEvent/CMXCPHits.h"

/** Container class for CMXCPHits objects */

using namespace LVL1;
typedef DataVector<CMXCPHits> CMXCPHitsCollection;

#endif
