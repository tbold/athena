#include "MuonCondTest/AlignCondAthTest.h"
#include "MuonCondTest/MuonDetectorStatusTest.h"
//#include "MuonCondTest/RpcStatusTest.h"
#include "MuonCondTest/MuonConditionsTestAlg.h" 
#include "MuonCondTest/MDT_DCSStatusTest.h"
#include "MuonCondTest/MDTConditionsTestAlg.h"
#include "MuonCondTest/RPCStatusTestAlg.h"
#include "MuonCondTest/MuonConditionsHistoSummary.h"

DECLARE_COMPONENT( AlignCondAthTest )
DECLARE_COMPONENT( MuonDetectorStatusTest )
//DECLARE_COMPONENT( RpcStatusTest )
DECLARE_COMPONENT( MuonConditionsTestAlg )
DECLARE_COMPONENT( MDT_DCSStatusTest )
DECLARE_COMPONENT( MDTConditionsTestAlg )
DECLARE_COMPONENT( RPCStatusTestAlg )
DECLARE_COMPONENT( MuonConditionsHistoSummary )

