/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
#include "GeoModelKernel/Units.h"

const double TileDetDescrManager::vBarrelCells[23] = {17799.76*GeoModelKernelUnits::cm3,   //A1
						      17799.76*GeoModelKernelUnits::cm3,   //A2
						      18459.01*GeoModelKernelUnits::cm3,   //A3
						      18459.01*GeoModelKernelUnits::cm3,   //A4
						      19777.51*GeoModelKernelUnits::cm3,   //A5
						      20436.76*GeoModelKernelUnits::cm3,   //A6
						      21096.01*GeoModelKernelUnits::cm3,   //A7
						      23073.76*GeoModelKernelUnits::cm3,   //A8
						      24392.26*GeoModelKernelUnits::cm3,   //A9
						      21096.01*GeoModelKernelUnits::cm3,   //A10
						      75726.60*GeoModelKernelUnits::cm3,   //BC1
						      77024.24*GeoModelKernelUnits::cm3,   //BC2
						      77024.24*GeoModelKernelUnits::cm3,   //BC3
						      81574.95*GeoModelKernelUnits::cm3,   //BC4
						      81574.95*GeoModelKernelUnits::cm3,   //BC5
						      88401.02*GeoModelKernelUnits::cm3,   //BC6
						      91974.02*GeoModelKernelUnits::cm3,   //BC7
						      91014.21*GeoModelKernelUnits::cm3,   //BC8
						      34219.80*GeoModelKernelUnits::cm3,   //B9
						      110000.61*GeoModelKernelUnits::cm3,  //D0
						      111375.62*GeoModelKernelUnits::cm3,  //D1
						      118250.66*GeoModelKernelUnits::cm3,  //D2
						      137500.77*GeoModelKernelUnits::cm3}; //D3

const double TileDetDescrManager::vExtendedCells[12] = {11863.88*GeoModelKernelUnits::cm3,   //A12
							32955.21*GeoModelKernelUnits::cm3,   //A13
							36909.84*GeoModelKernelUnits::cm3,   //A14
							39546.25*GeoModelKernelUnits::cm3,   //A15
							63274.01*GeoModelKernelUnits::cm3,   //A16
							44472.59*GeoModelKernelUnits::cm3,   //B11
							75047.49*GeoModelKernelUnits::cm3,   //B12
							83386.10*GeoModelKernelUnits::cm3,   //B13
							88945.18*GeoModelKernelUnits::cm3,   //B14
							97283.79*GeoModelKernelUnits::cm3,   //B15
							293772.18*GeoModelKernelUnits::cm3,  //D5
							338967.89*GeoModelKernelUnits::cm3}; //D6

const double TileDetDescrManager::vItcGapCells[6] = {13461.47*GeoModelKernelUnits::cm3,  //C10
						     46542.48*GeoModelKernelUnits::cm3,  //D4
						     1292.80*GeoModelKernelUnits::cm3,   //E1
						     1244.11*GeoModelKernelUnits::cm3,   //E2
						     776.24*GeoModelKernelUnits::cm3,    //E3
						     468.36*GeoModelKernelUnits::cm3};   //E4
